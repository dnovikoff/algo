#include "helper.hpp"

#include <algo/partition.hpp>

V test(const V& in) {
	V inCopy(in);
	std::cout << "partition "; My::print(inCopy);
	My::partition(inCopy.begin(), inCopy.end(),
		[](const int x) {
			return x % 2;
		}
	);
	return inCopy;
}

int testMiddle(const V& in) {
	V inCopy(in);
	return *My::partition(inCopy.begin(), inCopy.end(),
		[](const int x) {
			return x % 2;
		}
	);
}

BOOST_AUTO_TEST_CASE (empty) {
	VEQUALS(test({}), {});
}

BOOST_AUTO_TEST_CASE (oneTest) {
	VEQUALS(test({1}), {1});
	VEQUALS(test({1}), {1});
	VEQUALS(test({1}), {1});
}

BOOST_AUTO_TEST_CASE (multi1) {
	VEQUALS(test({1, 2, 3, 4, 5, 6}), V({1, 5, 3, 4, 2, 6}));
	VEQUALS(test({1, 3, 5, 2, 4, 6}), V({1, 3, 5, 2, 4, 6}));

	BOOST_CHECK_EQUAL(testMiddle({1, 2, 3, 4, 5, 6}), 4);
	BOOST_CHECK_EQUAL(testMiddle({1, 3, 5, 2, 4, 6}), 2);
}

