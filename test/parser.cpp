#include <sstream>
#include "helper.hpp"

#include <algo/parser.hpp>

static double parse(const std::string& input) {
	std::cout << "Parsing: " << input << std::endl;
	std::stringstream ss(input);
	My::Parser p(ss);
	double r = 0;
	try {
		r = p.parse();
	} catch (const My::Exception& e) {
		std::cout << "Exception: " << e.what() << std::endl;
		throw;
	}
	std::cout << input << " = " << r << std::endl;
	return r;
}

BOOST_AUTO_TEST_CASE (throwCases) {
	// Leading zeros are not allowed
	BOOST_CHECK_THROW(parse("00"), My::Exception);
	BOOST_CHECK_THROW(parse("01"), My::Exception);
	BOOST_CHECK_THROW(parse("001"), My::Exception);

	// Signs are not allowed with zeros
	BOOST_CHECK_THROW(parse("+0"), My::Exception);
	BOOST_CHECK_THROW(parse("-0"), My::Exception);

	// No trailing operations
	BOOST_CHECK_THROW(parse("6+"), My::Exception);
	BOOST_CHECK_THROW(parse("5-"), My::Exception);
	BOOST_CHECK_THROW(parse("6*"), My::Exception);
	BOOST_CHECK_THROW(parse("5/"), My::Exception);

	// No * and / at the number start
	BOOST_CHECK_THROW(parse("*6"), My::Exception);
	BOOST_CHECK_THROW(parse("/5"), My::Exception);

	// Two numbers, separated by space
	BOOST_CHECK_THROW(parse("5 6"), My::Exception);
	BOOST_CHECK_THROW(parse("(5 6)"), My::Exception);

	// This cases are not allowed
	BOOST_CHECK_THROW(parse("2-+2"), My::Exception);
	BOOST_CHECK_THROW(parse("2++2"), My::Exception);
	BOOST_CHECK_THROW(parse("2--2"), My::Exception);
	BOOST_CHECK_THROW(parse("2+-2"), My::Exception);

	// Empty cases
	BOOST_CHECK_THROW(parse(""), My::Exception);
	BOOST_CHECK_THROW(parse("          "), My::Exception);

	// Devision by zero
	BOOST_CHECK_THROW(parse("2/0"), My::Exception);
	BOOST_CHECK_THROW(parse("2/(2-1-1)"), My::Exception);

	//Wrong chars
	BOOST_CHECK_THROW(parse("a2"), My::Exception);
	BOOST_CHECK_THROW(parse("2a"), My::Exception);
	BOOST_CHECK_THROW(parse("(2)a"), My::Exception);
	BOOST_CHECK_THROW(parse("(2a)"), My::Exception);
	BOOST_CHECK_THROW(parse("(2 + hello)"), My::Exception);
}

BOOST_AUTO_TEST_CASE (wrongBraces) {
	BOOST_CHECK_THROW(parse("("), My::Exception);
	BOOST_CHECK_THROW(parse(")"), My::Exception);
	BOOST_CHECK_THROW(parse("()"), My::Exception);
	BOOST_CHECK_THROW(parse("(1"), My::Exception);
	BOOST_CHECK_THROW(parse("2)"), My::Exception);
	BOOST_CHECK_THROW(parse("(2+4))"), My::Exception);
	BOOST_CHECK_THROW(parse("((2+6)*4"), My::Exception);
	BOOST_CHECK_THROW(parse("((5+6)*4))"), My::Exception);
}

BOOST_AUTO_TEST_CASE (signedNumbers) {
	// Signs are allowed with numbers
	BOOST_CHECK_EQUAL(parse("+1"), 1);
	BOOST_CHECK_EQUAL(parse("-16"), -16);
	// Spaces are allowed between number and sign
	BOOST_CHECK_EQUAL(parse(" -  24"), -24);
}

BOOST_AUTO_TEST_CASE (lowPriorityOperations) {
	BOOST_CHECK_EQUAL(parse("2+1"), 3);
	BOOST_CHECK_EQUAL(parse("1+9-44"), -34);
	BOOST_CHECK_EQUAL(parse("2-1-1"), 0);
	BOOST_CHECK_EQUAL(parse("2-1-1-1"), -1);
}

BOOST_AUTO_TEST_CASE (hightPriorityOperations) {
	BOOST_CHECK_EQUAL(parse("6/3"), 2);
	BOOST_CHECK_EQUAL(parse("8/4*5"), 10);

	// Multiply on signed
	BOOST_CHECK_EQUAL(parse("6*-1"), -6);
	BOOST_CHECK_EQUAL(parse("6*+1"), +6);
}

BOOST_AUTO_TEST_CASE (operationMixWihtNoBraces) {
	BOOST_CHECK_EQUAL(parse("2+3*4"), 14);
	BOOST_CHECK_EQUAL(parse("2*3+4"), 10);
}

BOOST_AUTO_TEST_CASE (spaces) {
	BOOST_CHECK_EQUAL(parse(" \n\t  123"), 123);
	BOOST_CHECK_EQUAL(parse(" \n\t  4  \t\t    "), 4);
}

BOOST_AUTO_TEST_CASE (braces) {
	BOOST_CHECK_EQUAL(parse("(0)"), 0);
	BOOST_CHECK_EQUAL(parse("(12)"), 12);
	BOOST_CHECK_EQUAL(parse("(-124)"), -124);
	BOOST_CHECK_EQUAL(parse("(+36)"), 36);

	BOOST_CHECK_EQUAL(parse("(((42)))"), 42);
}

BOOST_AUTO_TEST_CASE (bracesComplex) {
	BOOST_CHECK_EQUAL(parse("(2+3)*4"), 20);
	BOOST_CHECK_EQUAL(parse("2*(3+4)"), 14);
}

BOOST_AUTO_TEST_CASE (zeroCases) {
	BOOST_CHECK_EQUAL(parse("0+2"), 2);
	BOOST_CHECK_EQUAL(parse("2+0"), 2);
	BOOST_CHECK_EQUAL(parse("2-0"), 2);
	BOOST_CHECK_EQUAL(parse("0-2"), -2);
	BOOST_CHECK_EQUAL(parse("2+0+0-0+0"), 2);
	BOOST_CHECK_EQUAL(parse("2*0"), 0);
	BOOST_CHECK_EQUAL(parse("2*0*0"), 0);
	BOOST_CHECK_EQUAL(parse("0/2"), 0);
}

BOOST_AUTO_TEST_CASE (numbers) {
#define MY_CHECK(N) BOOST_CHECK_EQUAL(parse(#N), N);
	MY_CHECK(1);
	MY_CHECK(10);
	MY_CHECK(42);
	MY_CHECK(3428);
	MY_CHECK(0);
#undef MY_CHECK
}
