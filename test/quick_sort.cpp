#include "helper.hpp"

#include <algo/quick_sort.hpp>
#include "sort_tester.hpp"

BOOST_AUTO_TEST_CASE (sortTest) {
	SortTester tester;
	tester.test([](const VectorType& v){
		std::cout << "Quick test: ";
		My::print(v);
		VectorType vCopy(v);
		My::qucikSort(vCopy.begin(), vCopy.end());
		std::cout << std::endl;
		return vCopy;
	});
}
