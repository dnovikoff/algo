#ifndef TEST_SORT_TESTER_HPP_
#define TEST_SORT_TESTER_HPP_

#include <test/helper.hpp>

struct SortTester {
	template<typename Functor>
	void test(Functor testSort) {
		VEQUALS(testSort({}), {});
		VEQUALS(testSort({1}), {1});

		VEQUALS(testSort({1, 2}), V({1, 2}));
		VEQUALS(testSort({2, 1}), V({1, 2}));

		VEQUALS(testSort({1, 2, 3}), V({1, 2, 3}));
		VEQUALS(testSort({3, 2, 1}), V({1, 2, 3}));
		VEQUALS(testSort({2, 1, 3}), V({1, 2, 3}));
		VEQUALS(testSort({1, 3, 2}), V({1, 2, 3}));
		VEQUALS(testSort({3, 1, 2}), V({1, 2, 3}));
		VEQUALS(testSort({2, 3, 1}), V({1, 2, 3}));

		VEQUALS(testSort({6, 5, 4, 3, 2, 1}), V({1, 2, 3, 4, 5, 6}));
		VEQUALS(testSort({5, 4, 3, 2, 1}), V({1, 2, 3, 4, 5}));
	}
};

#endif // TEST_SORT_TESTER_HPP_
