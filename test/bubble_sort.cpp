#include "helper.hpp"

#include <algo/bubble_sort.hpp>
#include "sort_tester.hpp"

BOOST_AUTO_TEST_CASE (sortTest) {
	SortTester tester;
	tester.test([](const VectorType& v){
		VectorType vCopy(v);
		My::bubbleSort(vCopy.begin(), vCopy.end());
		return vCopy;
	});
}
