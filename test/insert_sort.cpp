#include "helper.hpp"

#include <algo/insert_sort.hpp>
#include "sort_tester.hpp"

BOOST_AUTO_TEST_CASE (sortTest) {
	SortTester tester;
	tester.test([](const VectorType& v){
		VectorType vCopy(v);
		My::insertSort(vCopy.begin(), vCopy.end());
		return vCopy;
	});
}

BOOST_AUTO_TEST_CASE (insertRecTest) {
	SortTester tester;
	tester.test([](const VectorType& v){
		VectorType vCopy(v);
		My::insertSortRec(vCopy.begin(), vCopy.end());
		return vCopy;
	});
}
