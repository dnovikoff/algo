#include "helper.hpp"

#include <algo/merge.hpp>

VectorType testMerge(const VectorType& v1, const VectorType& v2) {
	VectorType buffer(v1.size()+v2.size(), 0);
	My::merge(v1.begin(), v1.end(), v2.begin(), v2.end(), buffer.begin());
	return buffer;
}

BOOST_AUTO_TEST_CASE (emptyMerge) {
	VEQUALS(testMerge({}, {}), {});
}

BOOST_AUTO_TEST_CASE (oneMerge) {
	VEQUALS(testMerge({1}, {2}), V({1, 2}));
	VEQUALS(testMerge({1}, {}), V({1}));
	VEQUALS(testMerge({}, {2}), V({2}));
	VEQUALS(testMerge({2}, {1}), V({1, 2}));
}

BOOST_AUTO_TEST_CASE (equalsMerge) {
	VEQUALS(testMerge({1, 2, 3}, {4, 5, 6}), V({1, 2, 3, 4, 5, 6}));
	VEQUALS(testMerge({4, 5, 6}, {1, 2, 3}), V({1, 2, 3, 4, 5, 6}));
	VEQUALS(testMerge({1, 3, 5}, {2, 4, 6}), V({1, 2, 3, 4, 5, 6}));
}

BOOST_AUTO_TEST_CASE (differentSizeMerge) {
	VEQUALS(testMerge({1, 2, 3}, {}), V({1, 2, 3}));
	VEQUALS(testMerge({4, 5, 6}, {1}), V({1, 4, 5, 6}));
	VEQUALS(testMerge({1, 3, 5}, {2, 4, 6, 8, 12}), V({1, 2, 3, 4, 5, 6, 8, 12}));
}

