#include <sstream>
#include <fstream>

#include "helper.hpp"

template<typename T>
void mergeStreams(std::istream& s1, std::istream& s2, std::ostream& out) {
	typedef std::istream_iterator<T> Iterator;
	typedef std::ostream_iterator<T> OutputIterator;
	const Iterator iend;
	std::merge(Iterator(s1), iend, Iterator(s2), iend, OutputIterator(out, " "));
}

std::string test(const std::string& str1, const std::string& str2) {
	std::stringstream s1(str1);
	std::stringstream s2(str2);

	std::ostringstream oss;
	mergeStreams<int>(s1, s2, oss);
	return oss.str();
}

template<typename T>
class IteratorAdapter: public std::iterator<std::input_iterator_tag, T, ptrdiff_t, const T*, const T&> {
	typedef IteratorAdapter<T> Self;
	union Data {
		T value;
		char buffer[sizeof(T)];
	};
public:
	// Constrctor for end iterator
	IteratorAdapter():stream(NULL), current() {
	}

	explicit IteratorAdapter(std::istream& i):stream(&i), current() {
		operator++();
	}

	const T& operator*() {
		if (!stream){
			throw std::runtime_error("Not allowed to dereference invalid iterator");
		}
		return current;
	}

	Self& operator++() {
		if (!stream){
			throw std::runtime_error("Not allowed do increment invalid iterator");
		}
		if (stream->eof() || stream->fail()) {
			stream = nullptr;
			return *this;
		}
		Data d;
		d.buffer[0] = stream->get();
		if (stream->eof()) {
			stream = nullptr;
			return *this;
		}
		stream->read(d.buffer+1, sizeof(T)-1);
		std::cout << "Buffer is " << std::string(d.buffer, sizeof(T)) << std::endl;
		std::cout << "Values is " << d.value << std::endl;
		std::cout << "fail()=" << stream->fail() << std::endl;
		std::cout << "eof()=" << stream->eof() << std::endl;
		if (stream->fail()) {
			throw std::runtime_error("Not enough bytes");
		}
		current = d.value;
		return *this;
	}

	bool operator==(const Self& rhs) const {
		return stream == rhs.stream;
	}

	bool operator!=(const Self& rhs) const {
		return !operator ==(rhs);
	}
private:
	std::istream* stream;
	T current;
};


BOOST_AUTO_TEST_CASE (ddd) {
	const std::string s1("1 6 18");
	const std::string s2("3 4 42 28 777");

	std::cout << "Resulting is " << test(s1, s2) << std::endl;
}

BOOST_AUTO_TEST_CASE (ddd1) {
	std::cout << "next test" << std::endl;
	typedef IteratorAdapter<int> iter;
	const std::string s("12345678");
	std::cout << "Sizeof = " << s.size() << std::endl;
	std::stringstream ss(s);
	iter iend;
	std::copy(iter(ss), iend, std::ostream_iterator<int>(std::cout, " "));
}
