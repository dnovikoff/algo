#include <iostream>
#include <sstream>
#include <algo/parser.hpp>

std::string readLine() {
	char buffer[256];
	std::cin.getline(buffer, sizeof(buffer)-1);
	return buffer;
}

bool isEmpty(std::istream& s) {
	while (!s.eof()) {
		const int c = s.get();
		if (!std::isspace(c)) {
			if (c == std::istream::traits_type::eof()) {
				break;
			}
			s.unget();
			return false;
		}
	}
	return true;
}

int main() {
	std::cout << "Type 'exit' to finish" << std::endl;

	while (true) {
		const std::string line = readLine();
		if (line == "exit") {
			break;
		}
		std::stringstream ss(line);
		if (isEmpty(ss)) {
			// Just empty line. Skip
			continue;
		}
		My::Parser parser(ss);
		try {
			const double result = parser.parse();
			std::cout << "Result = " << result << std::endl;
		} catch (const My::Exception& e) {
			std::cout << "Error parsing your expression: " << e.what() << std::endl;
		}
	}
	std::cout << "Bye ;)" << std::endl;
	return 0;
}
