#include "helper.hpp"

#include <algo/merge_sort.hpp>
#include "sort_tester.hpp"

BOOST_AUTO_TEST_CASE (sortTest) {
	SortTester tester;
	tester.test([](const VectorType& v){
		VectorType vCopy(v);
		VectorType buffer(v);
		My::mergeSort(vCopy.begin(), vCopy.end(), buffer.begin());
		return buffer;
	});
}
