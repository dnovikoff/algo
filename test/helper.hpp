#ifndef TEST_HELPER_HPP_
#define TEST_HELPER_HPP_

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN

#include <iostream>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <algo/print.hpp>

namespace Test {

template<typename C1, typename C2>
bool checkEquals(const C1& c1, const C2& c2) {
	if (c1.size() != c2.size()) return false;
	return std::equal(c1.begin(), c1.end(), c2.begin());
}

template<typename C1, typename C2>
bool cequals(const C1& c1, const C2& c2) {
	if (checkEquals(c1, c2)) return true;
	std::cout << "Comparation failed: " << std::endl;
	std::cout << "Left: ";
	My::print(c1);
	std::cout << "Right: ";
	My::print(c2);
	return false;
}

typedef std::vector<int> VectorType;

bool vequals(const VectorType& c1, const VectorType& c2) {
	return cequals(c1, c2);
}

} // namespace Test

typedef Test::VectorType VectorType;
typedef VectorType V;

#define CEQUALS(A, B) BOOST_CHECK(Test::cequals(A, B))
#define VEQUALS(A, B) BOOST_CHECK(Test::vequals(A, B))

#endif // TEST_HELPER_HPP_
