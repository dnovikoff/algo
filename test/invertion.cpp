#include "helper.hpp"

#include <algo/invertion.hpp>

using namespace My;

BOOST_AUTO_TEST_CASE (testEmpty) {
	BOOST_CHECK_EQUAL(countInvertionsN2(V()), 0u);
}

BOOST_AUTO_TEST_CASE (testOne) {
	BOOST_CHECK_EQUAL(countInvertionsN2(V({15})), 0);
}

BOOST_AUTO_TEST_CASE (testSorted) {
	BOOST_CHECK_EQUAL(countInvertionsN2(V({1, 2, 3, 4, 5, 6})), 0);
}

BOOST_AUTO_TEST_CASE (testReverse) {
	BOOST_CHECK_EQUAL(countInvertionsN2(V({6, 5, 4, 3, 2, 1})), 15u);
}

bool testInv(const VectorType& v) {
	const size_t expected = countInvertionsN2(v);
	VectorType vCopy(v);
	const size_t result = countInvertionsMerge(vCopy.begin(), vCopy.end());

	My::print(v);

	std::cout << "Invertion calculated by N^2   algo = " << expected << std::endl;
	std::cout << "Invertion calculated by Merge algo = " << result << std::endl;

	if (expected != result) {
		return false;
	}
	return true;
}

BOOST_AUTO_TEST_CASE (testCorrect) {
	BOOST_CHECK(testInv({}));
	BOOST_CHECK(testInv({1}));
	BOOST_CHECK(testInv({1, 2, 3, 4, 5, 6}));
	BOOST_CHECK(testInv({6, 5, 4, 3, 2, 1}));

	BOOST_CHECK(testInv({6, 5, 33, 11, 2, 1}));
	BOOST_CHECK(testInv({6, 5, 33, 11, 2, 1, 88, 0}));
	BOOST_CHECK(testInv({6, 5, 33, 11, 2, 1, 88}));

	BOOST_CHECK(testInv({1, 2}));
	BOOST_CHECK(testInv({2, 1}));
	BOOST_CHECK(testInv({1, 2, 3}));
	BOOST_CHECK(testInv({2, 3, 1}));
	BOOST_CHECK(testInv({3, 1, 2}));
	BOOST_CHECK(testInv({3, 2, 1}));
	BOOST_CHECK(testInv({2, 1, 3}));

	BOOST_CHECK(testInv({1, 2, 6, 4, 5, 3}));
	BOOST_CHECK(testInv({1, 2, 6, 4, 5, 3, 7, 8}));

	BOOST_CHECK(testInv({1, 2, 6, 4, 5, 7}));

	BOOST_CHECK(testInv({4, 1, 2, 3}));
	BOOST_CHECK(testInv({4, 1, 3, 2}));

	BOOST_CHECK(testInv({1, 3, 2, 4}));

	BOOST_CHECK(testInv({1, 2, 3, 4, 8, 7, 6, 5}));
	BOOST_CHECK(testInv({4, 3, 2, 1, 5, 6, 7, 8}));

	BOOST_CHECK(testInv({8, 7, 6, 5, 1, 2, 3, 4}));
	BOOST_CHECK(testInv({5, 6, 7, 8, 4, 3, 2, 1}));
}

