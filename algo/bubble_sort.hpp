#ifndef ALGO_BUBBLE_SORT_HPP_
#define ALGO_BUBBLE_SORT_HPP_

#include <algorithm>
#include <algo/print.hpp>

namespace My {

template<typename Iterator>
void bubbleSort(Iterator first, const Iterator last) {
	std::cout << "Bubble sort: "; My::print(first, last);

	if (first == last) return;
	auto last1 = last;
	--last1;

	for (auto i=first; i != last1; ++i) {
		auto j = i;
		++j;
		for (; j != last; ++j) {
			if (*j < *i) {
				std::iter_swap(i, j);
			}
		}
		std::cout << "After step: "; My::print(first, last);
	}

	std::cout << std::endl;
}

} // namespace My

#endif // ALGO_BUBBLE_SORT_HPP_
