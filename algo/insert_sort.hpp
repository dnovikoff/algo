#ifndef ALGO_INSERT_SORT_HPP_
#define ALGO_INSERT_SORT_HPP_

#include <algo/print.hpp>

namespace My {

template<typename Iterator>
void insertSort(const Iterator first, const Iterator last) {
	std::cout << "Insert sort: "; My::print(first, last);
	if (first == last) return;
	auto next = first;
	while (++next != last) {
		const auto item = *next;
		auto local = next;
		auto prev = local;
		while (local != first && item < *(--prev)) {
			*local = *prev;
			local = prev;
			std::cout << "SubStep: "; My::print(first, last);
		}
		*local = item;
		std::cout << "Step: "; My::print(first, last);
	}
	std::cout << std::endl;
}

template<typename Iterator>
void insertSortRec(Iterator first, Iterator last) {
	if (first == last) return;
	--last;
	const auto item = *last;
	if (first == last) return;
	insertSortRec(first, last);
	auto local = last;
	auto prev = local;
	while (local != first && item < *(--prev)) {
		*local = *prev;
		local = prev;
	}
	*local = item;
}


}  // namespace My

#endif // ALGO_INSERT_SORT_HPP_
