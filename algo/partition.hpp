#ifndef ALGO_PARTITION_HPP_
#define ALGO_PARTITION_HPP_

#include <algorithm>

namespace My {

template<typename Iterator, typename Functor>
Iterator partition(Iterator first, Iterator last, const Functor& f) {
	while (first != last) {
		while (f(*first)) {
			if (++first == last) return last;
		}
		do {
			--last;
			if (first == last) return first;
		} while (!f(*last));
		std::iter_swap(first, last);
	}
	return last;
}

} // namespace My

#endif // ALGO_PARTITION_HPP_
