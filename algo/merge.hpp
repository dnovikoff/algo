#ifndef ALGO_MERGE_HPP_
#define ALGO_MERGE_HPP_

namespace My {

template<typename Iterator, typename OutputIterator>
OutputIterator copy(Iterator first, const Iterator last, OutputIterator out) {
	while (first != last) {
		*(out++) = *(first++);
	}
	return out;
}

template<typename Iterator1, typename Iterator2, typename BufferIterator>
BufferIterator merge(
	Iterator1 first1, const Iterator1 last1,
	Iterator2 first2, const Iterator2 last2,
	BufferIterator out
) {
	while (first1 != last1 && first2 != last2) {
		*(out++) = (*first1 < *first2)? *(first1++) : *(first2++);
	}
	out = My::copy(first1, last1, out);
	out = My::copy(first2, last2, out);
	return out;
}

} // namespace My

#endif // ALGO_MERGE_HPP_
