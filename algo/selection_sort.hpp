#ifndef ALGO_SELECTION_SORT_HPP_
#define ALGO_SELECTION_SORT_HPP_

#include <algorithm>
#include <algo/print.hpp>

namespace My {

template<typename Iterator>
void selectionSort(Iterator first, const Iterator last) {
	std::cout << "Selection sort: "; My::print(first, last);

	if (first == last) return;
	auto last1 = last;
	--last1;

	for (auto i=first; i != last1; ++i) {
		auto j = i;
		auto min = i;
		++j;
		for (; j != last; ++j) {
			if (*j < *min) {
				min = j;
			}
		}
		std::iter_swap(i, min);
		std::cout << "After step: "; My::print(first, last);
	}

	std::cout << std::endl;
}

} // namespace My

#endif // ALGO_SELECTION_SORT_HPP_
