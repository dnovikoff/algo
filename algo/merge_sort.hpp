#ifndef ALGO_MERGE_SORT_HPP_
#define ALGO_MERGE_SORT_HPP_

#include <algorithm>
#include <algo/merge.hpp>
#include <algo/print.hpp>

namespace My {

template<typename Iterator, typename BufferIterator>
void mergeSort(
	Iterator first, const Iterator last,
	const BufferIterator bufferBegin
) {
	std::cout << "Sorting: "; My::print(first, last);
	const size_t size = std::distance(first, last);
	// Nothing to sort
	if (size < 2) return;
	size_t distance = 1;
	while (true) {
		size_t step = distance;
		distance *= 2;
		if (distance > size) return;
		const size_t itertations = size / distance ;
		auto out = bufferBegin;
		auto last1 = first;
		auto next = last1;
		for (size_t i=0; i<itertations; ++i) {
			next = last1;
			auto middle = next;
			std::advance(middle, step);
			last1 = middle;
			std::advance(last1, step);
			std::cout << "Merging "; print(next, middle);
			std::cout << "With "; print(middle, last1);
			auto tmpOut = out;
			out = My::merge(next, middle, middle, last1, out);
			std::cout << "Merge result is  "; print(tmpOut, out);
		}
		My::copy(bufferBegin, out, first);
		// merge tail
		if (last1 != last) {
			std::cout << "Need to merge tail "; print(last1, last);
			std::cout << "With "; print(next, last1);
			out = My::merge(next, last1, last1, last, bufferBegin);
			My::copy(bufferBegin, out, next);
		}

		std::cout << "After this step "; print(first, last);
	}

	std::cout << std::endl;
}

} // namespace My

#endif // ALGO_MERGE_SORT_HPP_
