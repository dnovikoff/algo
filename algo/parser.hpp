#ifndef ALGO_PARSER_HPP_
#define ALGO_PARSER_HPP_

#include <string>
#include <istream>
#include <stdexcept>

namespace My {

typedef std::runtime_error Exception;

class Parser {
public:
	explicit Parser(std::istream& s):stream(s) {
		buffer.reserve(10);
	}
	double parse() {
		return parseStart();
	}
private:
	double parseStart();

	/**
	 * Parse unsigned number
	 * Note, that zero number should be treated as a separate case
	 */
	double parseNumber();

	/**
	 * Parse parentheses expression
	 * Should be called after '(' symbol is already skipped
	 * Requires closing ')' at the end
	 */
	double parseBracketedExpression();

	/**
	 * Parse maximum possible sequence of operations
	 * Will end by eof or ')' or unexpected chars
	 */
	double parseExpression();

	// Will parse a braced expression or a number
	// In case of zeroAllowed is set to false, will throw exception if zero follows
	double parseNumberOrExpression(bool zeroAllowed = true);

	double parseLeft(const bool signAllowed = true);

	double parseUnsignedLeftAndResult() {
		const double left = parseLeft(false);
		return parseResult(left);
	}

	double parseResult(const double left);

	static double signedValue(const int sign, const double value) {
		return (sign=='-')?-value:value;
	}

	void throwErrorNear(const std::string& message) {
		throw Exception(message+ ". Near: '" + getTail() + "'");
	}

	// This function is used in diagnostic messages on parse failures
	std::string getTail() {
		std::string tail;
		do {
			const int c = getChar();
			if (!haveMoreData()) break;
			tail += c;
		} while (true);
		return tail;
	}

	int getChar() {
		if (!haveMoreData()) {
			return std::istream::traits_type::eof();
		}
		return stream.get();
	}

	int skipSpacesAndGetChar() {
		int c = 0;
		do {
			c = getChar();
		} while (std::isspace(c));
		return c;
	}

	bool haveMoreData() {
		return !stream.eof();
	}

	void ungetChar() {
		if (!haveMoreData()) {
			return;
		}
		stream.unget();
	}

	std::istream& stream;
	// The buffer is used for parsing numbers
	std::string buffer;
};

double Parser::parseNumber() {
	buffer.clear();
	int c = getChar();
	while (std::isdigit(c)) {
		buffer += c;
		c = getChar();
	}
	ungetChar();
	if (buffer.empty()) {
		throwErrorNear("Expected number");
	}
	return std::atoi(buffer.c_str());
}

// Will parse a braced expression or a number
// In case of zeroAllowed is set to false, will throw exception if zero follows
double Parser::parseNumberOrExpression(bool zeroAllowed) {
	const int c = skipSpacesAndGetChar();
	switch (c) {
		case '(':
			return parseBracketedExpression();
		case '0':
			if (!zeroAllowed) {
				throwErrorNear("Zero numbers not allowed here");
			}
			return 0; // will fail later if more digits follow
		default:
			break;
	}
	ungetChar();
	// Will fail if it is not number
	return parseNumber();
}

double Parser::parseLeft(const bool signAllowed) {
	const int c = skipSpacesAndGetChar();
	switch (c) {
		case '0':
			return 0;
		case '-':
		case '+':
			if (!signAllowed) {
				ungetChar();
				throwErrorNear("Sign not allowed here");
			}
			return parseResult(signedValue(c, parseNumberOrExpression(false)));
		case '(':
			return parseBracketedExpression();
		default:
			break;
	}
	ungetChar();
	return parseNumber();
}

double Parser::parseStart() {
	double r = parseExpression();
	if (haveMoreData()) {
		throwErrorNear("Unexpected chars at the end of the input");
	}
	return r;
}

double Parser::parseExpression() {
	double r = parseLeft();
	while (true) {
		const int c = skipSpacesAndGetChar();
		if (!haveMoreData()) return r;
		if (c == ')') {
			ungetChar();
			// should be handled by parent parser
			return r;
		}
		ungetChar();
		r = parseResult(r);
	}
	return r;
}

double Parser::parseBracketedExpression() {
	double r = parseExpression();
	if (skipSpacesAndGetChar() != ')') {
		throw std::runtime_error("Expected ')' at the end of the expression");
	}
	return r;
}

double Parser::parseResult(const double left) {
	const int c = skipSpacesAndGetChar();
	if (!haveMoreData()) return left;
	switch (c) {
		case '+':
		case '-':
			return left + parseResult(signedValue(c, parseLeft(false)));
		case '*':
			return left * parseLeft();
		case '/':
		{
			const double right = parseLeft();
			if (right == 0) {
				throwErrorNear("Devision by zero");
			}
			return left / right;
		}
		case ')':
			// Let this case be handled by upper parser
			ungetChar();
			return left;
		default:
			break;
	}
	throwErrorNear(std::string("Unexpected char '") + static_cast<char>(c) + "'");
	return 0;
}

} // namespace My

#endif // ALGO_PARSER_HPP_
