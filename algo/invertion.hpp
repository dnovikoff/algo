#ifndef ALGO_INVERTION_HPP_
#define ALGO_INVERTION_HPP_

#include <algo/merge.hpp>

namespace My {

// N^2 version to calculate number of invertions
// Will be used as test
template<typename Iterator>
size_t countInvertionsN2(Iterator first, Iterator last) {
	if (first == last) return 0;
	Iterator preLast = last;
	--preLast;
	if (preLast == first) return 0;

	size_t count = 0;
	for (; first != preLast; ++first) {
		Iterator next = first;
		++next;
		for (; next != last; ++next) {
			if (*next < *first) {
				++count;
			}
		}
	}
	return count;
}

template<typename Collection>
size_t countInvertionsN2(const Collection& c) {
	return countInvertionsN2(c.begin(), c.end());
}

template<typename Iterator1, typename Iterator2, typename BufferIterator>
size_t mergeAndCountInvertions(
	Iterator1 first1, const Iterator1 last1,
	Iterator2 first2, const Iterator2 last2,
	BufferIterator& out
) {
	size_t result = 0;
	size_t inverted = 0;
	while (true) {
		while (first1 != last1 && *first2 < *first1) {
			++inverted;
			*(out++) = *(first1++);
		}
		if (first1 == last1) break;
		while (first2 != last2 && *first1 < *first2) {
			result += inverted;
			*(out++) = *(first2++);
		}
		if (first2 == last2) break;
	}
	if (first1 != last1) {
		out = My::copy(first1, last1, out);
	} else {
		const size_t d = std::distance(first2, last2);
		result += inverted * d;
		out = My::copy(first2, last2, out);
	}
	return result;
}

template<typename Iterator>
size_t countInvertionsMerge(Iterator first, Iterator last) {
	size_t totalInvertions = 0;
	const size_t size = std::distance(first, last);
	// Nothing to sort
	if (size < 2) return 0;
	size_t distance = 1;
	VectorType buffer(size);
	const auto bufferBegin = buffer.begin();
	while (true) {
		size_t step = distance;
		distance *= 2;
		if (distance > size) return totalInvertions;
		const size_t itertations = size / distance ;
		auto out = bufferBegin;
		auto last1 = first;
		auto next = last1;
		for (size_t i=0; i<itertations; ++i) {
			next = last1;
			auto middle = next;
			std::advance(middle, step);
			last1 = middle;
			std::advance(last1, step);
			totalInvertions += My::mergeAndCountInvertions(next, middle, middle, last1, out);
		}
		My::copy(bufferBegin, out, first);
		// merge tail
		if (last1 != last) {
			auto tmpBuf = bufferBegin;
			totalInvertions += My::mergeAndCountInvertions(next, last1, last1, last, tmpBuf);
			My::copy(bufferBegin, tmpBuf, next);
		}
	}
	return totalInvertions;
}

} // namespace

#endif // ALGO_INVERTION_HPP_
