#ifndef ALGO_PRINT_HPP_
#define ALGO_PRINT_HPP_

namespace My {

template<typename Iterator>
void print(Iterator first, Iterator last) {
	bool flag = true;
	while (first != last) {
		if (flag) {
			flag = false;
		} else {
			std::cout << ", ";
		}
		std::cout << *(first++);
	}
	std::cout << std::endl;
}

template<typename C>
void print(const C& c) {
	print(c.begin(), c.end());
}

} // namespace My

#endif // ALGO_PRINT_HPP_
