#ifndef ALGO_QUICK_SORT_HPP_
#define ALGO_QUICK_SORT_HPP_

#include <algo/partition.hpp>
#include <algo/print.hpp>

namespace My {

template<typename Iterator>
void qucikSort(Iterator first, Iterator last) {
	const auto dist = std::distance(first, last);
	if (dist < 2u) return;
	std::cout << "Quick sort: ";
	print(first, last);
	const auto middleValue = *first;
	std::cout << "Middle value will be: " << middleValue << std::endl;
	typedef decltype(middleValue) ValueType;
	auto middle = My::partition(first, last, [middleValue](const ValueType& v) {
		return v < middleValue;
	}
	);
	// no luck =(
	if (middle == first) {
		++middle;
	}
	std::cout << "Partitioned: "; print(first, last);
	std::cout << "Partition middle is: " << *middle << std::endl;
	My::qucikSort(first, middle);
	My::qucikSort(middle, last);
}

} // namespace My

#endif // ALGO_QUICK_SORT_HPP_
